//BOTTON DARK THEME
$("#darkTrigger").click(function(){
    if ($("body").hasClass("dark")){
        $("body").removeClass("dark");
    }
    else{
        $("body").addClass("dark");
    }
});

$(document).ready(function () {
    var d = new Date();
    var n = d.getHours();

    if(n > 17 || n < 8){
        $("body").addClass("dark");
    }
});

//SCROLL UP
jQuery(function(){
    $(function () {
    $(window).scroll(function () { //Fonction appelée quand on descend la page
    if ($(this).scrollTop() > 50 ) {  // Quand on est à 50pixels du haut de page,
    $('#scrollUp').css('right','10px'); // Replace à 10pixels de la droite l'image
    } else { 
    $('#scrollUp').removeAttr( 'style' ); // Enlève les attributs CSS affectés par javascript
    }
    });
    });
    });

function up (){
    $("html, body").animate({scrollTop: 0},"slow");
}